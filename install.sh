#!/bin/bash

AnsibleConfigurationInstallation (){
    ansible-playbook ansible/tasks/do-config.yml
    echo "waiting k8s cluster to be running (kube-system)!"
    sleep 300   
    ansible-playbook ansible/tasks/helm.yml
    #timer to wait to tiller pod to be running
    echo "waiting to tiller pod to come up!"
    sleep 20
    ansible-playbook ansible/tasks/traefik.yml
    ansible-playbook ansible/tasks/monitoring.yml
    ansible-playbook ansible/tasks/todoapp.yml
    #wait to Digital LB to come up
    echo "waiting LB to come up"
    sleep 200
    ansible-playbook ansible/tasks/dnsrecords.yml
}

AnsibleConfigurationUninstall (){
    ansible-playbook ansible/tasks/do-config.yml 
    ansible-playbook ansible/tasks/deletednsrecords.yml
    ansible-playbook ansible/tasks/removetraefik.yml
    ansible-playbook ansible/tasks/removemonitoring.yml
    ansible-playbook ansible/tasks/removetodoapp.yml
    ansible-playbook ansible/tasks/deletehelm.yml
}

TerraformCreate (){
    cd terraform 
    terraform init
    terraform apply 
    cd ..
    # terraform apply -auto-approve
}

TerraformDelete (){
    cd terraform 
    terraform init
    terraform destroy 
    cd ..
    # terraform destroy -auto-approve
}


start=`date +%s`
if [ "$1" == "-h" ]; then
    echo "create k8s cluster (terraform/ansible)"
    echo "args: -i Install , -d Delete"
    echo "Usage: `basename $0` -i terraform"
    echo "Usage: `basename $0` -i ansible"
    echo "Usage: `basename $0` -d all"
    echo "Usage: `basename $0` -d terraform"
    echo "Usage: `basename $0` -d ansible"
    echo "Usage: `basename $0` -d all"
    # echo "by instance-id"
    # echo "Usage: `basename $0` -e i-xxxxxxxxxxxxxxxxx"
elif [ "$1" == "-i" ]; then
    if [ "$2" == "terraform" ]; then
        echo "terraform"
        TerraformCreate
    elif [ "$2" == "ansible" ]; then
        echo "ansible"
        AnsibleConfigurationInstallation
    elif [ "$2" == "all" ]; then
        echo "build all"
        TerraformCreate
        AnsibleConfigurationInstallation
    fi
elif [  "$1" == "-d" ]; then 
    if [ "$2" == "terraform" ]; then
        echo "terraform"
        TerraformDelete
    elif [ "$2" == "ansible" ]; then
        echo "ansible"
        AnsibleConfigurationUninstall
    elif [ "$2" == "all" ]; then
        echo "remove all"
        AnsibleConfigurationUninstall
        TerraformDelete
    fi  
fi
end=`date +%s`
runtime=$((end-start))
echo $runtime